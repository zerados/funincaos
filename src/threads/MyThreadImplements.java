package threads;

public class MyThreadImplements implements Runnable{

	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		String id = Thread.currentThread().getId() + "";
		System.err.println("hello from "+ name);
		System.err.println("and my name is " + id);
	}

}
