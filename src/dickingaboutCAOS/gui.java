package dickingaboutCAOS;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import jdk.nashorn.internal.runtime.regexp.joni.constants.Arguments;

import java.awt.Color;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.io.File;

public class gui extends JFrame{
	private JTextField binTextField;
	private JTextField decTextField;
	private JTextField hexTextField;
	public gui() {
		getContentPane().setBackground(Color.GRAY);
		setTitle("dunno");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Bin");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblNewLabel.setBounds(12, 13, 98, 32);
		getContentPane().add(lblNewLabel);
		
		JLabel lblDec = new JLabel("Dec");
		lblDec.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblDec.setBounds(12, 57, 53, 51);
		getContentPane().add(lblDec);
		
		JLabel lblHex = new JLabel("Hex");
		lblHex.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblHex.setBounds(12, 109, 53, 51);
		getContentPane().add(lblHex);
		
		binTextField = new JTextField();
		binTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				if (arg0.getKeyChar() == '0' || arg0.getKeyChar() == '1') {
					Long input = Long.parseLong(binTextField.getText() + arg0.getKeyChar(), 2);
					decTextField.setText(input +"");
					hexTextField.setText(Long.toHexString(input));
				}
				else if(arg0.getKeyChar() == KeyEvent.VK_BACK_SPACE || arg0.getKeyChar() == KeyEvent.VK_DELETE){
					if (binTextField.getText().length() <= 0) {
						decTextField.setText("");
						hexTextField.setText("");
					}
					else {
						Long input = Long.parseLong(binTextField.getText(), 2);
						decTextField.setText(input + "");
						hexTextField.setText(Long.toHexString(input));
					}
				}
				else {
					arg0.consume();
				}
			}
		});
		binTextField.setBounds(73, 23, 204, 22);
		getContentPane().add(binTextField);
		binTextField.setColumns(10);
		
		decTextField = new JTextField();
		decTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				if (arg0.getKeyChar() >= '0' && arg0.getKeyChar() <= '9') {
					Long input = Long.parseLong(decTextField.getText() + arg0.getKeyChar());
					binTextField.setText(Long.toBinaryString(input));
					hexTextField.setText(Long.toHexString(input));
				}
				else if(arg0.getKeyChar() == KeyEvent.VK_BACK_SPACE || arg0.getKeyChar() == KeyEvent.VK_DELETE){
					if (decTextField.getText().length() <= 0) {
						binTextField.setText("");
						hexTextField.setText("");
					}
					else {
						Long input = Long.parseLong(decTextField.getText());
						binTextField.setText(Long.toBinaryString(input));
						hexTextField.setText(Long.toHexString(input));
					}
				}
				else {
					arg0.consume();
				}	
				
			}
		});
		decTextField.setColumns(10);
		decTextField.setBounds(73, 76, 204, 22);
		getContentPane().add(decTextField);
		
		hexTextField = new JTextField();
		hexTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				if ((arg0.getKeyChar() >= '0' && arg0.getKeyChar() <= '9') || (arg0.getKeyChar() >= 'a' && arg0.getKeyChar() <=  'f')) {
					String input = hexTextField.getText() + arg0.getKeyChar();
					
					Long dec = Long.parseLong(input, 16);
					decTextField.setText(dec + "");
					binTextField.setText(Long.toBinaryString(dec));
				}
				else if(arg0.getKeyChar() == KeyEvent.VK_BACK_SPACE || arg0.getKeyChar() == KeyEvent.VK_DELETE){
					if (hexTextField.getText().length() <= 0) {
						decTextField.setText("");
						binTextField.setText("");
					}
					else {
						String input = hexTextField.getText();
						Long dec = Long.parseLong(input, 16);
						decTextField.setText(dec + "");
						binTextField.setText(Long.toBinaryString(dec));
					}
				}
				else {
					arg0.consume();
				}			
			}
		});
		hexTextField.setColumns(10);
		hexTextField.setBounds(73, 128, 204, 22);
		getContentPane().add(hexTextField);
		
		JButton btnReset = new JButton("reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				hexTextField.setText("");
				binTextField.setText("");
				decTextField.setText("");
			}
		});
		btnReset.setBounds(13, 182, 97, 25);
		getContentPane().add(btnReset);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 382, 353);
		getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		try {
			JLabel label = new JLabel(new ImageIcon(ImageIO.read(new File("img/vuze.png"))));
			panel.add(label, BorderLayout.CENTER);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		// TODO Auto-generated constructor stub
		setSize(new Dimension(400,400));	
		setVisible(true);
	}
}